# firebasevuechatv6

need:
gcloud firebasevuechatv6
remotejob/cloudfunction
remotejob/chatimgtrigger
remotejob/chatclientmsgtrigger

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).


firebase projects:list
npm install vue-scrollto --save
npm run build && firebase deploy

https://medium.com/@mattmaribojoc/6-vuejs-utility-libraries-to-jumpstart-your-project-e8f09a20c5b1

https://towardsdatascience.com/everyone-needs-their-own-chatbot-72cb210c0161

https://medium.com/better-programming/5-vs-code-extensions-thatll-change-your-dev-life-9786756a8121

https://medium.com/firebase-developers/should-i-query-my-firebase-database-directly-or-use-cloud-functions-fbb3cd14118c

https://chat.paratiisi.fi

nvm ls   #v13.5.0
firebase -V #7.11.0    #npm install -g -U firebase-tools
npm -v  #6.13.4



