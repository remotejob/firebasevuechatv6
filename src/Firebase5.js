import firebase from 'firebase/app';
import 'firebase';

const firebaseConfig = {
  apiKey: "AIzaSyBTFYXFsX5pFbNbGBr0GRey1CEtWoG7hbg",
  authDomain: "fir-vuechatv6.firebaseapp.com",
  databaseURL: "https://fir-vuechatv6.firebaseio.com",
  projectId: "fir-vuechatv6",
  storageBucket: "fir-vuechatv6.appspot.com",
  messagingSenderId: "914559889259",
  appId: "1:914559889259:web:3ae53d9c3492a72b3de62e",
  measurementId: "G-HTDJLF7K0R"
};


  const firebaseApp = firebase.initializeApp(firebaseConfig)
  
  export default firebaseApp